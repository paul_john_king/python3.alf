#!/usr/bin/env python3
# coding=utf-8

from subprocess import getoutput

def print_output_as_code(exec):
	return "\n".join(["    "+line for line in getoutput(exec).splitlines()])

with open("README.md","w") as file:
	file.write(
"""\
<!-- The `readme.py` Python script in the project's root directory generates
     this document. The next execution of the script will overwrite any changes
     to this document.  Please edit and execute the script in order to change
     this document. -->

{alf}

The `readme.py` script
----------------------

Execute the `readme.py` Python 3 script in the root directory of this project
to generate this document. The next execution of the script will overwrite any
changes made directly to this document, so please edit and execute the script
instead in order to edit this document.
""".format(
		alf=print_output_as_code(exec="./alf -h"),
	))
