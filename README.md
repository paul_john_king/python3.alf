<!-- The `readme.py` Python script in the project's root directory generates
     this document. The next execution of the script will overwrite any changes
     to this document.  Please edit and execute the script in order to change
     this document. -->

    usage: alf [-h] [-s «string»] [-e «string»] [-P «pattern»] [-L «level»] [-t]
               [-l] [-o] [-c «number»] [-m «pattern»] [-M «pattern»] [-n «string»]
               [-1 «string»] [-2] [-N «fore»:«back»] [-F1 «fore»:«back»]
               [-E1 «fore»:«back»] [-W1 «fore»:«back»] [-I1 «fore»:«back»]
               [-D1 «fore»:«back»] [-T1 «fore»:«back»] [-F2 «fore»:«back»]
               [-E2 «fore»:«back»] [-W2 «fore»:«back»] [-I2 «fore»:«back»]
               [-D2 «fore»:«back»] [-T2 «fore»:«back»] [-X1 «fore»:«back»]
               [-X2 «fore»:«back»] [--output-traceback]
               [«file» [«file» ...]]
    
    alf reads and parses appropriately formatted log4j entries from files or
    standard input, and writes selected, formatted and coloured (using ANSI escape
    character sequences or win32 calls if available) entry elements to standard
    output.  alf requires a Python 3 interpreter and the colorama Python 3 module.
    
    Each log4j entry should have the format
    
        «header»
        «message»
        «marker»
    
    where
    
        the entry header «header» comprises a single line of two or more fields all
        separated by a field separator,
    
        the header field separator is a string (' -=- ' by default),
    
        the first header field is a timestamp of the form 'YYYY-MM-DD HH:MM:SS,MMM',
    
        the second header field is a log4j level (one of 'FATAL', 'ERROR', 'WARN',
        'INFO', 'DEBUG' or 'TRACE'),
    
        the other header fields (if any) are strings that should not contain the
        field separator or the newline character,
    
        the entry message «message» comprises a number (possibly zero) of lines,
        and
    
        the message end marker «marker» is a string ('====' by default).
    
    Thus, a log4j entry with the default field separator and message end marker
    might look like
    
        2015-11-26 21:14:23,702 -=- INFO -=- fred.flinstone@bedrock.bc -=- 4768
        User [fred.flinstone@bedrock.bc] received token [4768].
        Set logging to DEBUG level for more information.
        ====
    
    A log file may also contain other messages not written by a properly configured
    log4j appender.  alf treats these as simple messages comprising just a number
    of lines without an entry header or a message end marker.
    
    alf parses log entries from files specified on the command line, or from
    standard input if no files are specified.  alf identifies the beginning of a
    log4j entry by recognising the contiguous timestamp, field separator and log4j
    level at the start of the message header, and identifies the end of the entry
    by recognising the message end marker or the beginning of a new log4j entry.
    Then, instructed by command-line options, alf prints some or all elements of
    some or all log entries to standard output, possibly inserting additional
    characters.
    
    positional arguments:
      «file»                read log entries from file «file» (or standard input
                            if no files are given)
    
    optional arguments:
      -h, --help            show this help message and exit
    
    input read options:
      -s «string», --field-separator «string»
                            use string «string» as the log4j header field
                            separator (default: ' -=- ')
      -e «string», --message-end-marker «string»
                            use string «string» as the log4j message end marker
                            (default: '====')
    
    output display options:
      -P «pattern», --timestamp-search-pattern «pattern»
                            print log4j entries with a timestamp that matches
                            regular expression «pattern»
      -L «level», --minimum-level «level»
                            print only log4j entries with level «level» or higher
                            (default: print all log4j entries and all messages)
      -t, --hide-timestamp-field
                            do not print the timestamp field of each log4j header
      -l, --hide-level-field
                            do not print the level field of each log4j header
      -o, --hide-other-fields
                            do not print the fields, other than timestamp and
                            level, of each log4j header
      -c «number», --hide-lines-after-count «number»
                            do not print more than the first «number» lines of
                            each message (default: no limit, print all lines)
      -m «pattern», --hides-lines-after-match «pattern»
                            if a line of a message matches regular expression
                            «pattern» then do not print the remaining lines of the
                            message
      -M «pattern», --hide-lines-from-match «pattern»
                            if a line of a message matches regular expression
                            «pattern» then do not print the line or the remaining
                            lines of the message
    
    output insertion options:
      -n «string», --null-field «string»
                            print string «string» to represent each empty field of
                            each log4j header (default: '')
      -1 «string», --line-prefix «string»
                            print string «string» before each line (and each
                            timestamp if the '-2' or '--timestamp-prefix' option
                            is used) of each message (default: '')
      -2, --timestamp-prefix
                            print the timestamp ('****-**-** **:**:**,***' if not
                            known) before the first line of each message, and
                            indent the other lines
    
    output colour options:
      -N «fore»:«back», --null-field-colors «fore»:«back», --null-field-colours «fore»:«back»
                            if the '-n' or '--null-field' option is used to print
                            a string to represent each empty field then print the
                            string with colour pair «fore»:«back» (see colour
                            pair, below)
      -F1 «fore»:«back», --fatal-entry-field-colors «fore»:«back», --fatal-entry-field-colours «fore»:«back»
      -E1 «fore»:«back», --error-entry-field-colors «fore»:«back», --error-entry-field-colours «fore»:«back»
      -W1 «fore»:«back», --warn-entry-field-colors «fore»:«back», --warn-entry-field-colours «fore»:«back»
      -I1 «fore»:«back», --info-entry-field-colors «fore»:«back», --info-entry-field-colours «fore»:«back»
      -D1 «fore»:«back», --debug-entry-field-colors «fore»:«back», --debug-entry-field-colours «fore»:«back»
      -T1 «fore»:«back», --trace-entry-field-colors «fore»:«back», --trace-entry-field-colours «fore»:«back»
                            print the fields and message line prefixes of each
                            FATAL, ERROR, WARN, INFO, DEBUG and TRACE level entry
                            respectively with colour pair «fore»:«back» (see
                            colour pair, below)
      -F2 «fore»:«back», --fatal-entry-line-colors «fore»:«back», --fatal-entry-line-colours «fore»:«back»
      -E2 «fore»:«back», --error-entry-line-colors «fore»:«back», --error-entry-line-colours «fore»:«back»
      -W2 «fore»:«back», --warn-entry-line-colors «fore»:«back», --warn-entry-line-colours «fore»:«back»
      -I2 «fore»:«back», --info-entry-line-colors «fore»:«back», --info-entry-line-colours «fore»:«back»
      -D2 «fore»:«back», --debug-entry-line-colors «fore»:«back», --debug-entry-line-colours «fore»:«back»
      -T2 «fore»:«back», --trace-entry-line-colors «fore»:«back», --trace-entry-line-colours «fore»:«back»
                            print the message lines (but not their prefixes) of
                            each FATAL, ERROR, WARN, INFO, DEBUG and TRACE level
                            entry respectively with colour pair «fore»:«back» (see
                            colour pair, below)
      -X1 «fore»:«back», --other-entry-field-colors «fore»:«back», --other-entry-field-colours «fore»:«back»
                            print the message line prefixes of each non-log4j
                            entry with colour pair «fore»:«back» (see colour pair,
                            below)
      -X2 «fore»:«back», --other-entry-line-colors «fore»:«back», --other-entry-line-colours «fore»:«back»
                            print the message lines (but not their prefixes) of
                            each non-log4j entry with colour pair «fore»:«back»
                            (see colour pair, below)
    
    debugging options:
      --output-traceback    print a Python traceback if an exception is caught
    
    A colour pair is a string with the format
    
        «for»:«back»
    
    where «fore» and «back» are colour strings from among 'black', 'red', 'green',
    'yellow', 'blue', 'magenta', 'cyan' and 'white' that specify respectively the
    foreground and background colours that alf should try to use to print an
    element.  The colour strings are case insensitive, but if the first character
    of «fore» is upper case then alf should try to use brighter colours.  If «fore»
    or «back» is empty then alf uses the current terminal foreground or background
    colour.  The simple colour «fore» is equivalent to the colour pair «fore»:.
    Thus,
    
        'white:blue' prints white on blue,
    
        'White:blue' prints brighter white on blue,
    
        ':blue' prints the current terminal foreground colour on blue,
    
        'white:' and 'white' both print white on the current terminal background
        colour, and
    
        ':' prints the current terminal foreground colour on the current terminal
        background colour (the default).

The `readme.py` script
----------------------

Execute the `readme.py` Python 3 script in the root directory of this project
to generate this document. The next execution of the script will overwrite any
changes made directly to this document, so please edit and execute the script
instead in order to edit this document.
